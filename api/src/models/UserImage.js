const mongoose = require("mongoose")
const userImageSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    fileName: {
        type: String,
        required: true
    },
    uploadTime: {
        type: Date,
        default: Date.now,
        required: true
    },
    isRecognized: {
        type: Boolean,
        default: false
    },
    fullTextAnnotation: {
        type: JSON,
        default: '{}',
        required: true
    }
})

const UserImage = mongoose.model('UserImage', userImageSchema)
module.exports = UserImage